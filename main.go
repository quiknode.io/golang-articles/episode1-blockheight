package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	flag "github.com/spf13/pflag"

	"github.com/ethereum/go-ethereum/ethclient"
	logging "github.com/op/go-logging"
)

var (
	logger          = logging.MustGetLogger("main")
	quikNodeURL     *string
	etherscanAPIKey *string
	nodeConnection  *ethclient.Client
	httpClient      = &http.Client{Timeout: 10 * time.Second}
)

func init() {
	var err error
	logFormatter := logging.MustStringFormatter(`%{color}%{time:15:04:05.000} %{shortfunc} ▶ %{level:.10s} %{id:03x}%{color:reset} %{message}`)
	logging.SetFormatter(logFormatter)
	consoleBackend := logging.NewLogBackend(os.Stdout, "", 0)
	consoleBackend.Color = true
	logging.SetLevel(logging.DEBUG, "main")

	quikNodeURL = flag.String("node", "", "The https url of your quicknode")
	etherscanAPIKey = flag.String("ethersanKey", "", "your etherscan API key")
	flag.Parse()

	if *quikNodeURL == "" {
		logger.Fatal("the --node command line argument is required")
	}
	if *etherscanAPIKey == "" {
		logger.Fatal("the --ethersanKey command line argument is required")
	}

	nodeConnection, err = ethclient.Dial(*quikNodeURL)
	if err != nil {
		logger.Fatalf("error connecting to node at: %s error was: %s", *quikNodeURL, err.Error())
	}
}

func main() {

	etherscanHeight, err := getEtherscanBlockHeight()
	if err != nil {
		logger.Errorf("unable to retrieve highest block number from etherscan, error was: %s", err.Error())
	} else {
		logger.Infof("Etherscan highest block in the main net: %d", etherscanHeight)
	}
	quikNodeHeight, err := getQuicknodeBlockHeight()
	if err != nil {
		logger.Errorf("unable to retrieve highest block number from quiknode, error was: %s", err.Error())
	} else {
		logger.Infof("Quiknode highest block in the main net : %d", quikNodeHeight)
	}
}

func getQuicknodeBlockHeight() (uint64, error) {
	var err error
	var heighestBlock uint64

	header, err := nodeConnection.HeaderByNumber(context.Background(), nil)
	if err != nil {
		return heighestBlock, err
	}
	h, _ := strconv.Atoi(fmt.Sprintf("%s", header.Number.String()))
	heighestBlock = uint64(h)
	return heighestBlock, err
}

// EtherscanBlockNumberResponse json/struct representation of a call to etherscan: "eth_blockNumber"
type EtherscanBlockNumberResponse struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  string `json:"result"`
}

func getEtherscanBlockHeight() (uint64, error) {
	var (
		esr         EtherscanBlockNumberResponse
		err         error
		blockHeight uint64
	)

	url := fmt.Sprintf("https://api.etherscan.io/api?module=proxy&action=eth_blockNumber&apikey=%s", *etherscanAPIKey)
	r, err := httpClient.Get(url)
	if err != nil {
		return blockHeight, err
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return blockHeight, err
	}

	fmt.Printf("API Raw Body: %+v\n", r.Body)

	err = json.Unmarshal(body, &esr)
	if err != nil {
		return blockHeight, err
	}

	fmt.Printf("DUMP: %+v\n", esr)

	bn, err := strconv.ParseInt(esr.Result, 0, 64)
	blockHeight = uint64(bn)
	return blockHeight, err
}
