# block-height

Get the current block height for an ethereum node, written in Golang

## Clone & Go

```
git clone git@gitlab.com:quiknode.io/golang-articles/episode1-blockheight.git
cd episode1-blockheight
go run main.go --node="<YOUR_QUIKNODE_URL>" --ethersanKey="<YOUR_ETHERSCAN_API_KEY>"

```