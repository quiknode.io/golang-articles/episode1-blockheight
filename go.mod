module gitlab.com/quiknode.io/golang-articles/episode1-blockheight

require (
	github.com/allegro/bigcache v1.2.0 // indirect
	github.com/aristanetworks/goarista v0.0.0-20190311233030-eca2dc3c65b8 // indirect
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/ethereum/go-ethereum v1.8.23
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/rs/cors v1.6.0 // indirect
	github.com/spf13/pflag v1.0.3
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20190313024323-a1f597ede03a // indirect
	golang.org/x/net v0.0.0-20190313220215-9f648a60d977 // indirect
)
